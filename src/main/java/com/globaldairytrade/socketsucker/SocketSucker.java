package com.globaldairytrade.socketsucker;

import java.net.HttpCookie;
import java.net.URI;
import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.websocket.client.ClientUpgradeRequest;
import org.eclipse.jetty.websocket.client.WebSocketClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SocketSucker {

  private static final Logger logger = LoggerFactory.getLogger(SocketSucker.class);
  private static final String XSRF_TOKEN = "X-XSRF-TOKEN";
  private static final String JSESSIONID = "JSESSIONID";
  private static String webSocketUrl;
  private static String xsrfToken;
  private static String sessionId;

  public static void main(String[] args) {
    SocketSucker socketSucker = new SocketSucker();
    WebSocketClient client;

    if (args != null && args.length >= 3) {
      getParamsFromArgs(args);
    } else {
      getParamsFromUser();
    }

    if (webSocketUrl.length() > 0 && xsrfToken.length() > 0 && sessionId.length() > 0) {
      logger.debug("Using webSocketUrl={}, xsrfToken={}, sessionId={}", new Object[] {webSocketUrl, xsrfToken, sessionId});
      if (webSocketUrl.startsWith("wss")) {
        SslContextFactory sslContextFactory = new SslContextFactory();
        client = new WebSocketClient(sslContextFactory);
      } else {
        client = new WebSocketClient();
      }
      ClarityWebSocketListener clarityWebSocketListener = new ClarityWebSocketListener(socketSucker);
      HttpCookie sessionCookie = new HttpCookie(JSESSIONID, sessionId);
      HttpCookie xsrfCookie = new HttpCookie(XSRF_TOKEN, xsrfToken);

      try {
        client.start();
        ClientUpgradeRequest clientUpgradeRequest = new ClientUpgradeRequest();
        clientUpgradeRequest.setCookies(Arrays.asList(sessionCookie, xsrfCookie));
        URI webSocketUri = new URI(webSocketUrl);
        client.connect(clarityWebSocketListener, webSocketUri, clientUpgradeRequest);
        clarityWebSocketListener.awaitClose(5, TimeUnit.SECONDS);
      }
      catch (Exception e) {
        logger.error("Exception starting SocketSucker", e);
        System.exit(1);
      }
    }
  }

  private static void getParamsFromArgs(String[] args) {
    for (String arg : args) {
      if (arg.startsWith("-url=")) {
        webSocketUrl = arg.substring("-url=".length());
      } else if (arg.startsWith("-token=")) {
        xsrfToken = arg.substring("-token=".length());
      } else if (arg.startsWith("-sessionId=")) {
        sessionId = arg.substring("-sessionId=".length());
      }
    }
  }

  private static void getParamsFromUser() {
    Scanner keyboard = new Scanner(System.in);
    System.out.println("Enter (or paste) the WebSocket URI to connect to: ");
    webSocketUrl = getWebSocketUrl(keyboard);
    System.out.println("Paste in the value of your XSRF token: ");
    xsrfToken = getXsrfToken(keyboard);
    System.out.println("Paste in the value of your JSESSIONID: ");
    sessionId = getSessionId(keyboard);
  }

  private static String getWebSocketUrl(Scanner keyboard) {
    String webSocketUrl = keyboard.nextLine();
    if (!webSocketUrl.startsWith("ws://") && !webSocketUrl.startsWith("wss://")) {
      System.out.println("The WebSocket URI must start with ws:// or wss://");
      System.exit(1);
    }
    return webSocketUrl;
  }

  private static String getXsrfToken(Scanner keyboard) {
    return keyboard.nextLine();
  }

  private static String getSessionId(Scanner keyboard) {
    return keyboard.nextLine();
  }

  public void receivedFinalMessage() {
    System.out.println("Received final message. Exiting now...");
    System.exit(0);
  }
}
