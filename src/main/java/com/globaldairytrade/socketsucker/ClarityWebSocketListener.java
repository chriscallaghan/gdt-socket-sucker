package com.globaldairytrade.socketsucker;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.globaldairytrade.common.services.v1x0.dto.DataSetDetails;
import com.globaldairytrade.common.services.v1x0.dto.EventRoundCalculations;
import com.globaldairytrade.common.services.v1x0.dto.EventRoundStatusDetails;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.WebSocketListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClarityWebSocketListener implements WebSocketListener {

  private static final Logger logger = LoggerFactory.getLogger(ClarityWebSocketListener.class);
  private static final int MAX_TEXT_MESSAGE_SIZE = 26214400;

  private final CountDownLatch closeLatch;
  private Session session;
  private ObjectMapper objectMapper;
  private SocketSucker socketSucker;

  public ClarityWebSocketListener(SocketSucker socketSucker) {
    this.closeLatch = new CountDownLatch(1);
    this.objectMapper = new ObjectMapper();
    this.socketSucker = socketSucker;
  }

  public boolean awaitClose(int duration, TimeUnit timeUnit) throws InterruptedException {
    return this.closeLatch.await(duration, timeUnit);
  }

  public void onWebSocketBinary(byte[] payload, int offset, int len) {
    System.out.println("Eurgh, binary...");
  }

  public void onWebSocketText(String message) {
    long now = System.currentTimeMillis();
    if (message.length() == 1) {
      System.out.println("ping");
    } else if (message.startsWith("{\"EventName\":") && message.substring(22, 40).contains("CurrentRound")) {
      try {
        DataSetDetails dataSetDetails = objectMapper.readValue(message, DataSetDetails.class);
        System.out.println("Processing Event " + dataSetDetails.getEventName());
      }
      catch (IOException e) {
        logger.error("IOException parsing initial message", e);
      }
    } else if (message.startsWith("{\"EventName\":") && message.substring(22, 40).contains("RoundNumber")) {
      try {
        EventRoundCalculations calculations = objectMapper.readValue(message, EventRoundCalculations.class);
        if (calculations.getRoundNumber() == 0) {
          System.out.println("Round " + calculations.getRoundNumber() + " calculations received");
        } else {
          long timeTaken = now - calculations.getReceivedTimestamp().getTime();
          System.out.println("Round " + calculations.getRoundNumber() + " calculations received in " + timeTaken + " milliseconds");
        }
        if (calculations.getFinalRound()) {
          System.out.println("Final round received");
          this.session.close(0, "Final round received");
          this.socketSucker.receivedFinalMessage();
        }
      }
      catch (IOException e) {
        logger.error("IOException parsing round calculations", e);
      }
    } else if (message.startsWith("{\"TradingEventId\":")) {
      try {
        EventRoundStatusDetails statusDetails = objectMapper.readValue(message, EventRoundStatusDetails.class);
        System.out.println(statusDetails.getStatusMessage());
      }
      catch (IOException e) {
        logger.error("IOException parsing status message", e);
      }
    }
  }

  public void onWebSocketClose(int statusCode, String reason) {
    System.out.println("Closing because of " + reason);
    this.session = null;
    this.closeLatch.countDown();
  }

  public void onWebSocketConnect(Session session) {
    this.session = session;
    session.getPolicy().setMaxTextMessageSize(MAX_TEXT_MESSAGE_SIZE);
    session.getPolicy().setMaxTextMessageBufferSize(MAX_TEXT_MESSAGE_SIZE);
    System.out.println("Connected");
  }

  public void onWebSocketError(Throwable cause) {
    System.out.println("Error!");
    System.out.println(cause.getMessage());
  }
}
